# Shuttle

**This is an experimental library for internal use that helps you quickly create RPC services without unnecessary code.
RabbitMQ is used as a transport.**

Это экспериментальная библиотека для внутреннего использования, помогает быстро создавать RPC сервисы без лишнего кода.
В качестве транспорта используется RabbitMQ, ничего другого на данный момент не планируется.

Основная идея.

На "сервере":
- Пользователь описывает логику сервиса в виде класса в typescript
- Публичные методы помечает декоратором @method
- Отгружает это в Shuttle.addAction()
- Shuttle публикует нужные очереди и прочее и обеспечивает удаленный вызов методов

На "клиенте":
- Пользователь получает дефинишены (это можно сделать разными способами)
- Получает экземпляр удаженного "класса" через Shuttle.getRemoteAction()
- Вызывает нужные ему методы и получает результат как обычно
- Подписывается на события
- IDE дает подсказочки, ts следит за простыми ошибками т.к. есть дефинишены


## Как начинать

### Установка

```bash
npm i @sash/shuttle

yarn add @sash/shuttle
```



------ Надо бы здесь что-то про конфигурацию ...

Для того чтобы начать, нужно создать экземпляр своего класса и отправить его в Shuttle:

```typescript

export class SomeClass {

  protected factor = 5;

  @method()
  public async methodName(x: number, y: number): Promise<number> {
    return this.sum();
  }

  private sum(x: number, y: number): number {
    return (parseInt(x) + parseInt(y)) * this.factor;
  }
}


const someObject = new SomeClass();
const shuttle = new Shuttle();

shuttle.addAction('ActionName', someObject);
shuttle.begin();

```

Код на клиентской стороне при этом буде выглядеть так:

```typescript

const shuttle = new Shuttle();

const main = async () => {
  const action = await shuttle.getRemoteAction<SomeClass>('ActionName');
  const result = await action.methodName(2, 3);
};

```


## Описание понятий




## Описание компонентов

- [Shuttle](./docs/Shuttle.md)
- [ShuttleConnect](./docs/ShuttleConnect.md)
- [logging](./docs/logging.md)
