import { EventEmitter } from 'events';

import { ShuttleConnect } from './adapters/ShuttleConnect';
import type { ConnectionItem } from './adapters/ConnectionItem';
import { ShuttleActionLifeCycle } from './ShuttleActionLifeCycle';
import { ShuttleConfig } from './adapters/ShuttleConfig';
import { ShuttleRemoteActionBackbone } from './ShuttleRemoteActionBackbone';
import { ShuttleTrip } from './ShuttleTrip';
import { info } from './logging';
import { actionConfigStore } from './utils/actionConfigStore';

export class Shuttle extends EventEmitter {

  protected actions: {[name: string]: ShuttleActionLifeCycle} = {};
  protected remoteActions: {[name: string]: ShuttleRemoteActionBackbone} = {};
  protected remoteActionsWaitList: {[name: string]: any} = {};
  protected connectionItem!: ConnectionItem;

  public constructor(private readonly shuttleConfig?: ShuttleConfig) {
    super();
  }

  public async begin(): Promise<void> {
    if (!this.connectionItem) {
      this.emit('begin', this);
      this.connectionItem = await ShuttleConnect.getConnection(this.shuttleConfig);

      for (const actionLifeCycle of Object.values(this.actions)) {
        await actionLifeCycle.begin();
        this.emit('actionReady', this, actionLifeCycle);
      }
      for (const remoteActionName of Object.keys(this.remoteActionsWaitList)) {
        await this.prepareRemoteAction(remoteActionName, this.remoteActionsWaitList[remoteActionName]);
      }
      this.emit('ready', this);
    }
  }

  public async addAction(name: string, action: any): Promise<void> {
    if (this.actions[name]) {
      throw new Error(`The action ${name} already exists.`)
    }
    const actionLifeCycle = new ShuttleActionLifeCycle(name, action, this);
    this.actions[name] = actionLifeCycle;
    this.emit('actionAdded', this, name, action);
    info(`Shuttle: action ${name} type ${action.constructor.name} added.`);

    actionConfigStore.init(action);

    if (this.connectionItem) {
      await actionLifeCycle.begin();
      this.emit('actionReady', this, actionLifeCycle);
    }
  }

  public getActionLifeCycleManager(name: string): ShuttleActionLifeCycle | null {
    return this.actions[name] || null;
  }

  public addWaitListRemoteActions(name: string, params?: any) {
    if (this.connectionItem) {
      throw new Error('There is no way to change the waiting list after ready.');
    }
    this.remoteActionsWaitList[name] = params;
  }

  /**
   * Подготовка образа экшена.
   * Этот метод запросит на сервере всё необходимое для создания клиентского экшена.
   *
   * Если не передан params образ для создания будет сохранен в памяти.
   * Иначе будет обязательно запрашиваться.
   *
   * @param name - имя экшена
   * @param params - это дополнительные параметры попадут они в хуки типа onClientConnect
   */
  public async prepareRemoteAction(name: string, params?: any): Promise<ShuttleRemoteActionBackbone> {

    if (params !== undefined) {
      return this.createRemoteActionBackbone(name, params);
    }

    if (!this.remoteActions[name]) {
      this.remoteActions[name] = await this.createRemoteActionBackbone(name);
    }
    return this.remoteActions[name];
  }

  public async createRemoteActionBackbone(name: string, params?: any): Promise<ShuttleRemoteActionBackbone> {
    if (!this.connectionItem) {
      await this.begin();
    }
    const bb = new ShuttleRemoteActionBackbone(name, this.connectionItem, this, [params]);
    await bb.init();
    info(`Shuttle: created backbone for remote action ${name}.`);
    return bb;
  }

  public getConnection(): ConnectionItem {
    return this.connectionItem;
  }

  public async trip(): Promise<ShuttleTrip> {
    await this.begin();
    return new ShuttleTrip(this);
  }

}

export interface Shuttle {
  on(eventName: 'begin', listener: (shuttle: Shuttle) => void): this;
  on(eventName: 'actionAdded', listener: (shuttle: Shuttle, name: string, action: any) => void): this;
  on(eventName: 'actionReady', listener: (shuttle: Shuttle, alc: ShuttleActionLifeCycle) => void): this;
  on(eventName: 'ready', listener: (shuttle: Shuttle) => void): this;
  addListener(eventName: 'begin', listener: (shuttle: Shuttle,) => void): this;
  addListener(eventName: 'actionAdded', listener: (shuttle: Shuttle, name: string, action: any) => void): this;
  addListener(eventName: 'actionReady', listener: (shuttle: Shuttle, alc: ShuttleActionLifeCycle) => void): this;
  addListener(eventName: 'ready', listener: (shuttle: Shuttle) => void): this;
}
