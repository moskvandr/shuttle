import { EventEmitter } from 'events';
import * as rfdc from 'rfdc';

import { ShuttleMethodLifeCycle } from './ShuttleMethodLifeCycle';
import type { Shuttle } from './Shuttle';
import type { ConnectionItem } from './adapters/ConnectionItem';
import { ShuttleEvent } from './ShuttleEvent';
import { info } from './logging';
import { actionConfigStore } from './utils/actionConfigStore';
import {
  ActionConfiguration,
  ActionConfigurationHooks,
  ActionConfigurationParams,
  ActionManifest,
  ActionStuffing,
} from './interfaces';

const clone = rfdc({circles: false, proto: false});

export class ShuttleActionLifeCycle extends EventEmitter {

  private readonly actionMethods: ShuttleMethodLifeCycle[] = [];
  private isReady = false;

  constructor(public readonly name: string, private readonly action: ActionStuffing, private readonly shuttle: Shuttle) {
    super();
  }

  public async begin() {
    if (!this.isReady) {
      await this.prepareManifest();
      this.emit('begin', this);
      await this.pullHook('onBegin', this);
      for (const mlc of this.actionMethods) {
        await mlc.begin();
      }
      actionConfigStore.build(this.action);
      this.isReady = true;
      this.emit('ready', this);
      await this.pullHook('onReady', this);
      this.info(`Action is ready.`);
    }
  }

  public getAction(): ActionStuffing {
    return this.action;
  }

  public getShuttle(): Shuttle {
    return this.shuttle;
  }

  public getActionManifest(cloneManifest = false): ActionManifest {
    const manifest = actionConfigStore.getManifest(this.action);
    return cloneManifest ? clone(manifest) : manifest;
  }

  public getActionConfiguration(): Partial<ActionConfiguration> {
    return actionConfigStore.getActionConfiguration(this.action);
  }

  public getActionConfigurationParams(): ActionConfigurationParams {
    const params: ActionConfigurationParams = actionConfigStore.getActionConfiguration(this.action);
    const conn = this.getConnection();

    if (!params.timeout && conn.settings.timeout) {
      params.timeout = conn.settings.timeout;
    }

    return params;
  }

  public getConnection(): ConnectionItem {
    return this.shuttle.getConnection();
  }

  public async onContextReady(action: ActionStuffing): Promise<void> {
    this.emit('onContextReady', this, action);
    await this.pullHook('onContextReady', this, action);
  }

  protected async prepareManifest(): Promise<void> {
    this.addGetActionManifestMethod();
    const actionManifest = this.getActionManifest();
    actionManifest.actionConfiguration = this.getActionConfigurationParams();
    for (const methodName of Object.keys(actionManifest.methods)) {
      this.actionMethods.push(new ShuttleMethodLifeCycle(methodName, this));
    }
    await this.initEvents();
  }

  protected async initEvents(): Promise<void> {
    const actionManifest = this.getActionManifest();
    for (const [propertyName, propertyValue] of Object.entries(this.action)) {
      if (propertyValue instanceof ShuttleEvent) {
        actionManifest.events[propertyName] = {template: propertyValue.template};
        await propertyValue.init(propertyName, this.name, this.getConnection());
      }
    }
    this.info(`Events are ready.`);
  }

  protected addGetActionManifestMethod() {
    const actionManifest = this.getActionManifest();
    actionManifest.methods[':getActionManifest'] = {args: [], contextDisabled: true};

    this.action[':getActionManifest'] = async (params?: any): Promise<ActionManifest> => {
      const copyOfManifest = this.getActionManifest(true);
      this.emit('onClientConnect', this, copyOfManifest, params);
      await this.pullHook('onClientConnect', this, copyOfManifest, params);
      return copyOfManifest;
    }
  }

  protected async pullHook<K extends keyof ActionConfigurationHooks>(name: K, ...args: Parameters<ActionConfigurationHooks[K]>): Promise<void> {
    const actionCfg = this.getActionConfiguration();
    if (actionCfg && actionCfg[name] && typeof actionCfg[name] === 'function') {
      this.info(`Hook ${name} start.`);
      const fn = actionCfg[name] as any;
      await fn(...args);
      this.info(`Hook ${name} end.`);
    }
  }

  private info(msg: string) {
    info(`ShuttleActionLifeCycle:${this.name}: ${msg}`);
  }

}

export interface ShuttleActionLifeCycle {
  on(eventName: 'begin', listener: (acl: ShuttleActionLifeCycle) => void): this;
  on(eventName: 'ready', listener: (acl: ShuttleActionLifeCycle) => void): this;
  on(eventName: 'onClientConnect', listener: (acl: ShuttleActionLifeCycle, manifest: ActionManifest, clientParams?: any) => void): this;
  on(eventName: 'onContextReady', listener: (acl: ShuttleActionLifeCycle, action: ActionStuffing) => void): this;
  addListener(eventName: 'begin', listener: (acl: ShuttleActionLifeCycle) => void): this;
  addListener(eventName: 'ready', listener: (acl: ShuttleActionLifeCycle) => void): this;
  addListener(eventName: 'onClientConnect', listener: (acl: ShuttleActionLifeCycle, manifest: ActionManifest, clientParams?: any) => void): this;
  addListener(eventName: 'onContextReady', listener: (acl: ShuttleActionLifeCycle, action: ActionStuffing) => void): this;
}

