import type { OutgoingEvent } from './adapters/OutgoingEvent';
import type { IncomingEvent, IncomingEventHandler } from './adapters/IncomingEvent';
import type { ConnectionItem } from './adapters/ConnectionItem';

import { info } from './logging';
import type { ShuttleMessage } from './adapters/ShuttleMessage';
import { ActionStuffing } from './interfaces';

export type ShuttleEventHandler<T> = (data: T, action: any) => void;

export class ShuttleEvent<T> {

  public eventName!: string;
  public action!: ActionStuffing;
  protected serverAdapter!: OutgoingEvent;
  // protected clientAdapter!: IncomingEvent;
  protected clientAdapters?: Map<string, IncomingEvent>;
  protected conn!: ConnectionItem;
  protected actionName!: string;
  protected mode: 'server' | 'client' = 'server';
  protected packStore!: Record<string, any>;
  protected mapListeners = new Map<ShuttleEventHandler<T>, IncomingEventHandler>();


  // шаблон для публикации и подписки
  constructor(public readonly template: string[], action?: any, packStore?: Record<string, any>) {
    if (template.length < 1) {
      throw new Error('ShuttleEvent template is empty.');
    }
    if (action) {
      if (!packStore) {
        throw new Error('ShuttleEvent client mode requires packStore.');
      }
      this.action = action;
      this.mode = 'client';
      this.packStore = packStore;
      this.clientAdapters = new Map<string, IncomingEvent>();
    }
  }

  public async init(eventName: string, actionName: string, conn: ConnectionItem): Promise<void> {
    this.eventName = eventName;
    this.actionName = actionName;
    this.conn = conn;
    if (this.mode === 'server') {
      this.serverAdapter = await conn.createOutgoingEvent(`${actionName}:${eventName}`);
    }
  }

  public async on(listener: ShuttleEventHandler<T>, routingKey?: string): Promise<void> {
    if (this.mode === 'server') {
      throw new Error(`Shuttle: ShuttleEvent.on method is only allowed in client mode.`);
    }
    const handler = (msg: ShuttleMessage) => listener(msg.getPayload(), this.action);
    const key = routingKey || this.getRoutingKey();
    const adapter = await this.getOrCreateClientAdapter(key);
    adapter.addListener(handler);
    this.mapListeners.set(listener, handler);
  }

  public async emit(data: T, recipientRoutingKey?: string): Promise<boolean> {
    if (this.mode === 'client') {
      throw new Error(`Shuttle: ShuttleEvent.emit method is only allowed in server mode.`);
    }
    const routingKey = recipientRoutingKey || this.getRoutingKey();
    this.info(`Fire ${this.eventName} routing key is "${routingKey}"`);
    return this.serverAdapter.emit(data, routingKey);
  }

  public removeListener(listener: ShuttleEventHandler<T>, routingKey?: string): boolean {
    if (this.mode === 'server') {
      throw new Error(`Shuttle: ShuttleEvent.removeListener method is only allowed in client mode.`);
    }

    const key = routingKey || this.getRoutingKey();

    if (this.hasClientAdapter(key) && this.mapListeners.has(listener)) {
      const adapter = this.getClientAdapter(key);
      const result = adapter.removeListener(this.mapListeners.get(listener)!); // todo объективно это совсем ебанина, надо однажды что-то с этим сделать (основная проблема в том что внутри адаптера хочется отвязаться от экшенов)
      if (result) {
        this.mapListeners.delete(listener);
      }
      return result;
    }
    return false;
  }

  public async finish(): Promise<void> {
    const all: Promise<any>[] = [];
    if (this.clientAdapters) {
      this.clientAdapters.forEach((adapter) => {
        try {
          all.push(adapter.finish());
        } catch (e: any) {
          if (e.name !== 'IllegalOperationError') {
            throw e;
          }
        }
      });
    }
    await Promise.all(all);
  }

  protected hasClientAdapter(routingKey: string): boolean {
    return !!this.clientAdapters?.has(routingKey);
  }

  protected getClientAdapter(routingKey: string): IncomingEvent {
    if (!this.hasClientAdapter(routingKey)) {
      throw new Error(`Shuttle: ShuttleEvent.getClientAdapter incoming event adapter not found.`);
    }
    return this.clientAdapters?.get(routingKey)!;
  }

  protected async getOrCreateClientAdapter(routingKey: string): Promise<IncomingEvent> {
    if (!this.clientAdapters) {
      throw new Error(`Shuttle: ShuttleEvent.getOrCreateClientAdapter method is only allowed in client mode.`);
    }

    if (!this.clientAdapters.has(routingKey)) {
      this.clientAdapters.set(
        routingKey,
        await this.conn.createIncomingEvent(
          `${this.actionName}:${this.eventName}`,
          routingKey,
          this.packStore),
      );
    }

    return this.clientAdapters.get(routingKey)!;
  }

  protected getRoutingKey(): string {
    const result: string[] = this.template.map<string>((n) => n[0] === ':' ? this.getValueByPath(n) : n);
    return result.join('.');
  }

  private getValueByPath(path: string): string {
    const result = path.substring(1).split('.').reduce((o = {}, key) => o[key], this.action as any);
    if (result === undefined) {
      throw new Error(`ShuttleEvent: Could not find value ${path}`);
    }
    return result.toString();
  }

  private info(msg: string) {
    info(`ShuttleEvent:${this.action.constructor.name}:${this.eventName}: ${msg}`)
  }
}

export interface ShuttleEvent<T> {
  on(listener: (data: T, action: any) => void, routingKey?: string): Promise<void>;
}



