import { ShuttleEvent } from './ShuttleEvent';
import type { ConnectionItem } from './adapters/ConnectionItem';
import type { ShuttleRemoteActionBackbone } from './ShuttleRemoteActionBackbone';

export class ShuttleEventClientPack {

  private readonly packStore: Record<string, any> = {':onDestroy': []};
  private eventProviders = new Map<string, ShuttleEvent<any>>();

  constructor(private readonly conn: ConnectionItem, private readonly backbone: ShuttleRemoteActionBackbone) {
  }

  public async init(actionProxy: any) {
    for (const [eventName, eventManifest] of Object.entries(this.backbone.manifest.events)) {
      const e = new ShuttleEvent(eventManifest.template, actionProxy, this.packStore);
      await e.init(eventName, this.backbone.name, this.conn);
      this.eventProviders.set(eventName, e);
    }
  }

  public get(key: string): ShuttleEvent<any> | undefined {
    return this.eventProviders.get(key);
  }

  public has(key: string): boolean {
    return this.eventProviders.has(key);
  }

  public async finish(): Promise<void> {
    for (const [key, event] of this.eventProviders.entries()) {
      await event.finish();
      this.eventProviders.delete(key);
    }
  }

}
