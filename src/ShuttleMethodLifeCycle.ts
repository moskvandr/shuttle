import { EventEmitter } from 'events';

import { IncomingCall } from './adapters/IncomingCall';
import { createServerActionProxy } from './utils/createServerActionProxy';
import type { ShuttleActionLifeCycle } from './ShuttleActionLifeCycle';
import { ShuttleMessage } from './adapters/ShuttleMessage';
import { info } from './logging';
import { actionConfigStore } from './utils/actionConfigStore';
import { MethodManifest, MethodConfigurationHooks, ActionStuffing, MethodConfiguration  } from './interfaces';


export class ShuttleMethodLifeCycle extends EventEmitter {

  private readonly action: ActionStuffing;
  private readonly actionMethodInfo: MethodManifest;
  private readonly actionMethodConfiguration: Partial<MethodConfiguration>;
  private adapter!: IncomingCall;
  private isReady = false;

  constructor(public readonly name: string, private actionLifeCycle: ShuttleActionLifeCycle) {
    super();
    this.action = actionLifeCycle.getAction();
    this.actionMethodInfo = actionConfigStore.getManifestMethod(this.action, name);
    this.actionMethodConfiguration = actionConfigStore.getMethodConfigurationByName(this.action, name) || {};
  }

  public async begin() {
    if (!this.isReady) {
      this.emit('begin', this);
      await this.pullHook('onBegin', this);
      const conn = this.actionLifeCycle.getConnection();
      this.adapter = await conn.createIncomingCall(
        `${this.actionLifeCycle.name}:${this.name}`,
        async (msg) => await this.callActionMethod(msg),
      );
      this.isReady = true;
      this.emit('ready', this);
      await this.pullHook('onBegin', this);
      this.info(`Method is ready.`);
    }
  }

  public getActionLifeCycle(): ShuttleActionLifeCycle {
    return this.actionLifeCycle;
  }

  public getArgumentNames(): string[] {
    return actionConfigStore.getManifestMethod(this.action, this.name)?.args || [];
  }

  protected contextDisabled(): boolean {
    return !!this.actionMethodInfo.contextDisabled;
  }

  protected async callActionMethod(msg: ShuttleMessage): Promise<void> {
    this.info(`Method call action method.`);
    const { meta } = msg.get();
    let stepName = 'preparing context';
    try {
      const context = await this.recoveryContext(msg);

      stepName = 'creating proxy';
      const action = this.getProxy(context);

      stepName = 'context ready';
      await this.actionLifeCycle.onContextReady(action);

      stepName = 'getting arguments';
      const callParams = await this.getCallParams(msg, action);

      this.info(`Call params is ready: ${callParams}`);

      stepName = 'calling method';
      const payload = await action[this.name](...callParams);

      this.info(`Call is done.`);

      stepName = 'preparing reply context';
      const replyContext = this.getReplyContext(action);

      stepName = 'reply';
      await this.adapter.reply(msg, {meta, payload, context: {...replyContext}});
      this.info(`Method sent reply.`);
    } catch (e) {
      const errPrefix = `${this.action.constructor.name}:${this.name} ${stepName}`;
      const payload = e instanceof Error ? {...e, message: `${errPrefix} -> ${e.message}`, name: e.name, stack: e.stack} : e ;
      await this.adapter.exception(msg, {meta, payload, context: {}});
      this.info(`Method sent error.`);
    }
    msg.ack();
  }

  protected async recoveryContext(msg: ShuttleMessage): Promise<Record<string, any>> {

    if (this.contextDisabled()) {
      this.info(`Skip context recovery.`);
      return {};
    }

    const all: Promise<any>[] = [];
    const contextConfig = actionConfigStore.getContextConfiguration(this.action);
    const contextData = msg.getContext();

    Object.entries(contextConfig).forEach(([propertyName, config]) => {
      if (config.schema) {
        all.push(config.schema.parseAsync(contextData[propertyName]).then((v) => contextData[propertyName] = v));
      }
    });

    await Promise.all(all);
    this.info(`Context is valid.`);
    return contextData;
  }

  protected getReplyContext(action: any): Record<string, any> {
    const contextConfig = actionConfigStore.getContextConfiguration(this.action);
    const result: Record<string, any> = {};
    Object.entries(contextConfig).forEach(([propertyName, config]) => {
      if (config.reply) {
        result[propertyName] = action[propertyName];
      }
    });
    this.info(`Prepared reply context: ${Object.keys(result)}.`);
    return result;
  }

  protected async pullHook<K extends keyof MethodConfigurationHooks>(name: K, ...args: Parameters<MethodConfigurationHooks[K]>): Promise<void> {
    if (this.actionMethodConfiguration[name]) {
      this.info(`Hook ${name} start.`);
      await (this.actionMethodConfiguration[name] as any)(...args);
      this.info(`Hook ${name} end.`);
    }
  }

  private getProxy(context: Record<string, any>) {
    this.info(`Prepare proxy action.`);
    return createServerActionProxy(
      this.actionLifeCycle.getActionManifest(),
      this.actionLifeCycle.getAction(),
      context,
    );
  }

  private async getCallParams(msg: ShuttleMessage, action: any): Promise<any[]> {
    const params = msg.getPayload() || [];
    return this.validateAndTransformCallParams(params, action);
  }

  private async validateAndTransformCallParams(callParams: any[], action: any): Promise<any[]> {
    if (this.actionMethodConfiguration.paramsValidator) {
      this.info(`Validate call params.`);
      const result = await this.actionMethodConfiguration.paramsValidator(
        callParams,
        action,
        this,
      );
      return result || callParams;
    }
    return callParams;
  }

  private info(msg: string) {
    info(`ShuttleMethodLifeCycle:${this.actionLifeCycle.name}:${this.name}: ${msg}`);
  }

}

export interface ShuttleMethodLifeCycle {
  on(eventName: 'begin', listener: (mcl: ShuttleMethodLifeCycle) => void): this;
  on(eventName: 'ready', listener: (mcl: ShuttleMethodLifeCycle) => void): this;
  addListener(eventName: 'begin', listener: (mcl: ShuttleMethodLifeCycle) => void): this;
  addListener(eventName: 'ready', listener: (mcl: ShuttleMethodLifeCycle) => void): this;
}
