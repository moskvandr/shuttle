import type { OutgoingCall } from './adapters/OutgoingCall';
import type { ConnectionItem } from './adapters/ConnectionItem';
import type { Shuttle } from './Shuttle';
import { ActionManifest } from './interfaces';


export class ShuttleRemoteActionBackbone {

  public readonly adapters = new Map<string, OutgoingCall>();
  public manifest!: ActionManifest;
  protected readonly context: Record<string, any> = {};

  constructor(public readonly name: string, public readonly conn: ConnectionItem, protected readonly shuttle: Shuttle, protected readonly params?: any[]) {
  }

  public getDefaultContext(): Record<string, any> {
    return this.context;
  }


  public async init() {
    const clientAdapter = await this.conn.createOutgoingCall(`${this.name}::getActionManifest`);
    this.manifest = await clientAdapter.call(this.params, {}, null);
    await this.initMethods();
    this.initContext();
  }

  public getContext(propertyName: string): any {
    return this.context[propertyName];
  }

  public contextPropertyIsReadOnly(name: string): boolean {
    const pm = this.manifest.properties[name];
    if (!pm) {
      return false;
    }
    return !!pm.readonlyOnClient;
  }

  public contextPropertyIsShareOnReply(name: string): boolean {
    const pm = this.manifest.properties[name];
    if (!pm) {
      return true;
    }
    return !!pm.shareOnReply;
  }

  protected async initMethods() {
    for (const [methodName, methodManifest] of Object.entries(this.manifest.methods)) {
      const timeout = methodManifest.timeout || this.manifest.actionConfiguration?.timeout;
      this.adapters.set(methodName, await this.conn.createOutgoingCall(`${this.name}:${methodName}`, timeout));
    }
  }

  protected initContext() {
    for (const [propertyName, info] of Object.entries(this.manifest.properties)) {
      // eslint-disable-next-line no-prototype-builtins
      if (info.hasOwnProperty('defaultValue')) {
        this.context[propertyName] = info.defaultValue;
      }
    }
  }



}
