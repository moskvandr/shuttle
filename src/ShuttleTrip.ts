import { createClientActionProxy } from './utils/createClientActionProxy';
import { info } from './logging';
import type { Shuttle } from './Shuttle';
import type { ConnectionItem } from './adapters/ConnectionItem';
import type { ShuttleRemoteActionBackbone } from './ShuttleRemoteActionBackbone';

export class ShuttleTrip {

  private readonly sharedContext: Record<string, any> = {};
  private readonly remoteActions: Array<{proxy: any; back: ShuttleRemoteActionBackbone}> = [];
  private readonly connectionItem: ConnectionItem;

  constructor(private readonly shuttle: Shuttle) {
    this.connectionItem = shuttle.getConnection();
  }

  /**
   * Создание клиентского экшена.
   * @param name - имя экшена, как указано при добавлении в shuttle.addAction()
   * @param context - значения полей экшена, часть из них отпраивтся на сервер
   * @param params
   */
  public async createRemoteAction<T extends Record<string, any>>(name: string, context?: Record<string, any> | Partial<T>, params?: any): Promise<T> {
    info(`ShuttleTrip: try get remote action ${name}.`);

    const back = await this.shuttle.prepareRemoteAction(name, params);
    const proxy = await createClientActionProxy<T>(this.connectionItem, back, context || {}, this);

    this.remoteActions.push({proxy, back});

    Object.keys(this.sharedContext).forEach((propertyName) => {
      this.setSharedContextValue(propertyName, proxy, back);
    });

    info(`ShuttleTrip: remote action ${name} is ready.`);
    return proxy;
  }

  public async createRemoteActionExclusive<T extends Record<string, any>>(name: string, context?: Record<string, any>, params?: any): Promise<T> {
    info(`ShuttleTrip: try get exclusive remote action ${name}.`);
    const bb = await this.shuttle.createRemoteActionBackbone(name, params);
    const proxy = createClientActionProxy<T>(this.connectionItem, bb, context || {});
    info(`ShuttleTrip: exclusive remote action ${name} is ready.`);
    return proxy;
  }

  public async finish(remoteAction: any): Promise<void> {
    const actionFinish = remoteAction[':finish'];
    if (typeof actionFinish !== 'function') {
      throw new Error('I want a remote action');
    }
    info(`ShuttleTrip: finish remote action.`);
    return actionFinish();
  }

  public shareContext(propertyName: string, value: any) {
    info(`ShuttleTrip: share context ${propertyName} value: ${value}.`);
    this.sharedContext[propertyName] = value;
    this.remoteActions.forEach(({proxy, back}) => {
      this.setSharedContextValue(propertyName, proxy, back);
    });
  }

  private setSharedContextValue(propertyName: string, proxy: any, back: ShuttleRemoteActionBackbone) {
    if (back.contextPropertyIsShareOnReply(propertyName)) {
      proxy[`#share:${propertyName}`] = this.sharedContext[propertyName];
    }
  }

}
