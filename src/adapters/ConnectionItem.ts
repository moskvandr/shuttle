import type { Settings } from './Settings';
import type { OutgoingCall } from './OutgoingCall';
import type { IncomingCall } from './IncomingCall';
import type { ShuttleMessage } from './ShuttleMessage';
import type { OutgoingEvent } from './OutgoingEvent';
import type { IncomingEvent } from './IncomingEvent';


export interface ConnectionItem {
  url: string;
  settings: Settings;
  createOutgoingCall(exchange: string, timeout?: number): Promise<OutgoingCall>;
  createIncomingCall(exchange: string, onMessage: (message: ShuttleMessage) => void): Promise<IncomingCall>;
  createOutgoingEvent(exchange: string): Promise<OutgoingEvent>;
  createIncomingEvent(exchange: string, rotingKey: string, packStore: Record<string, any>, onMessage?: (message: ShuttleMessage) => void): Promise<IncomingEvent>;
  disconnect(): Promise<void>;
}

export type connectionItemFactory = (url: string, settings: Settings) => Promise<ConnectionItem>;
