// eslint-disable-next-line @typescript-eslint/no-empty-interface
import { ShuttleMessageData } from './ShuttleMessageData';
import { ShuttleMessage } from './ShuttleMessage';

export interface IncomingCall {
  reply(requestMessage: ShuttleMessage, message: ShuttleMessageData<any>): Promise<any>;
  exception(requestMessage: ShuttleMessage, message: ShuttleMessageData<any>): Promise<any>;
}
