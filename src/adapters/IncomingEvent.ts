import type { ShuttleMessage } from './ShuttleMessage';

export type IncomingEventHandler = (message: ShuttleMessage) => void | Promise<void>;

export interface IncomingEvent {
  addListener(listener: IncomingEventHandler): void;
  removeListener(listener: IncomingEventHandler): boolean;
  finish(): Promise<void>;
}
