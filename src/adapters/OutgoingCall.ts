export interface OutgoingCall {
  call(payload: any, context: Record<string, any>, action: any): Promise<any>;
}
