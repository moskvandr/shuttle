export interface OutgoingEvent {
  emit(payload: any, routingKey: string): Promise<boolean>;
}
