export class ResponseTimeoutError extends Error {
  message = 'The response timeout has expired.';
}
