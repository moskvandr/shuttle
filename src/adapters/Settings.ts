export interface Settings {
  prefetchCount?: number;
  reactionName?: string;
  timeout?: number;
}
