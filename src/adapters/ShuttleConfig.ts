import { URL } from 'url';

import { Settings } from './Settings';

export interface ShuttleConfig extends URL {
  server?: 'rabbitmq';
  settings?: Settings;
}
