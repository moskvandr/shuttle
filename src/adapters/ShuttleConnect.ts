// import { connect, Connection } from 'amqplib';
import { URL } from 'url';
import * as config from 'config';

import { error, info } from '../logging';
import type { ConnectionItem, connectionItemFactory } from './ConnectionItem';
import { getReactionByName } from '../reaction';
import { Settings } from './Settings';
import { ShuttleConfig } from './ShuttleConfig';
import { createConnectionItem } from './rabbitmq';

export class ShuttleConnect {

  protected static readonly connectionItemFactories = new Map<string, connectionItemFactory>([
    ['rabbitmq', createConnectionItem] // todo need to get rid of it here
  ]);
  protected static readonly connections = new Map<string, ConnectionItem>();

  static async getConnection(shuttleConfig?: ShuttleConfig): Promise<ConnectionItem> {
    const connectionUrl = this.getUrl(shuttleConfig);

    if (this.connections.has(connectionUrl)) {
      return this.connections.get(connectionUrl)!;
    }

    const settings = shuttleConfig && shuttleConfig?.settings || {};
    info(`Shuttle: start connecting to ${connectionUrl}`);

    const adapterName = shuttleConfig?.server ? shuttleConfig.server : 'rabbitmq';
    const connFactory = this.connectionItemFactories.get(adapterName);

    if (!connFactory) {
      throw new Error(`ConnectionItem factory ${adapterName} not found.`);
    }

    return this.addConnection(await connFactory(connectionUrl, this.prepareSettings(settings)));
  }

  static async disconnect(connectionUrl: string) {
    const connectionItem = this.connections.get(connectionUrl)!;
    try {
      await connectionItem.disconnect();
    } catch (e) {
      error(e);
    }
  }

  protected static getUrl(url?: ShuttleConfig | string): string {
    if (!url) {
      const resultUrl = new URL('amqp://rabbitmq');
      Object.assign(resultUrl, config.get<URL>('mq'));
      return resultUrl.toString();
    }
    return url.toString();
  }

  protected static async addConnection(item: ConnectionItem): Promise<ConnectionItem> {
    this.connections.set(item.url, item);
    this.addConnectionHandlers(item.url, item.settings);
    return item;
  }

  protected static addConnectionHandlers(url: string, settings: Settings) {
    const item = this.connections.get(url)!;

    const reactionClass = getReactionByName(settings.reactionName!);
    new reactionClass(item, () => {
      this.connections.delete(url);
    });
  }

  protected static prepareSettings(cfg: Settings): Settings {
    const prefetchCount = this.getConfig('mq.settings.prefetchCount', 10);
    const reactionName = this.getConfig('mq.settings.reaction', 'rabbitmq-default');
    return {prefetchCount, reactionName, ...cfg};
  }

  protected static getConfig<T>(path: string, defaultValue: T): T {
    return config.has(path) ? config.get<T>(path) : defaultValue ;
  }

}
