export abstract class ShuttleMessage {

  public abstract get(): any;

  public abstract getPayload(): any;

  public abstract getContext(): Record<string, any>;

  public abstract ack(): void;

  public abstract nack(): void;

}
