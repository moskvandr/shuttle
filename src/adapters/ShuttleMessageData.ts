export interface ShuttleMessageData<T> {
  meta: {
    transactionId: string;
    replyRoutingKey?: string;
    traceId?: string;
  };
  payload: T;
  context: Record<string, any>;
}
