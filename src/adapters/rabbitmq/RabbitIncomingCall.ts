import { Message } from 'amqplib';

import type { ShuttleMessage } from '../ShuttleMessage';
import { IncomingCall } from '../IncomingCall';

import { ShuttleMessageData } from '../ShuttleMessageData';
import { Subscription } from './Subscription';
import { RabbitMqConnectionItem } from './RabbitMqConnectionItem';
import { RabbitShuttleMessage } from "./RabbitShuttleMessage";

export class RabbitIncomingCall extends Subscription implements IncomingCall {

  constructor(
    protected readonly conn: RabbitMqConnectionItem,
    protected readonly exchange: string,
    protected readonly onMessage: (message: ShuttleMessage) => void
  ) {
    super(conn, exchange);
  }

  public async reply(requestMessage: ShuttleMessage, message: ShuttleMessageData<any>) {
    const data = requestMessage.get();
    const content = Buffer.from(JSON.stringify(message));
    this.channel.publish(this.replyExchangeFanout, data.meta.replyRoutingKey, content);
  }

  public async exception(requestMessage: ShuttleMessage, message: ShuttleMessageData<any>) {
    const data = requestMessage.get();
    const content = Buffer.from(JSON.stringify(message));
    this.channel.publish(this.exceptionExchangeFanout, data.meta.replyRoutingKey, content);
  }

  protected async initQueue(): Promise<void> {
    this.queue = `${this.callExchangeFanout}:queue`;

    await this.channel.assertQueue(this.queue, this.queueOptions);
    await this.channel.bindQueue(this.queue, this.callExchange, '');

    const { consumerTag } = await this.channel.consume(this.queue, async (msg: Message | null) => {
      if (msg) {
        const shuttleMessage = new RabbitShuttleMessage(this.channel, msg, consumerTag);
        this.onMessage(shuttleMessage);
      }
    }, {});
  }

}
