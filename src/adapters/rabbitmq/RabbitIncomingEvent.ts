import { v4 as uuid } from 'uuid';
import { Message } from 'amqplib';

import { IncomingEvent, IncomingEventHandler } from '../IncomingEvent';
import { info } from '../../logging';

import { Subscription } from './Subscription';
import { RabbitMqConnectionItem } from './RabbitMqConnectionItem';
import { RabbitShuttleMessage } from './RabbitShuttleMessage';

export class RabbitIncomingEvent extends Subscription implements IncomingEvent {

  protected listeners = new Set<IncomingEventHandler>();

  constructor(
    protected readonly conn: RabbitMqConnectionItem,
    protected readonly exchange: string,
    protected readonly routingKey: string,
    protected readonly packStore: Record<string, any>,
    onMessage?: IncomingEventHandler
  ) {
    super(conn, exchange);
    if (onMessage) {
      this.listeners.add(onMessage);
    }
  }

  public addListener(listener: IncomingEventHandler) {
    this.listeners.add(listener);
  }

  public removeListener(listener: IncomingEventHandler): boolean {
    return this.listeners.delete(listener);
  }

  protected async setChannel() {
    if (!this.packStore.channel) {
      this.packStore.channel = await this.conn.connection.createChannel();
    }
    this.channel = this.packStore.channel;
  }

  protected setExchangeName() {
    this.callExchangeFanout = `${this.exchange}:event`;
    this.callExchange = `${this.callExchangeFanout}:topic`;
  }

  protected async initExchange() {
    await this.assertExchanges(this.callExchangeFanout, this.callExchange, 'topic');
  }

  protected async initQueue(): Promise<void> {
    this.queue = `${this.callExchangeFanout}:queue:${uuid()}`;

    await this.channel.assertQueue(this.queue, this.queueOptions);
    await this.channel.bindQueue(this.queue, this.callExchange, this.routingKey);
    info(`Shuttle: asserted queue ${this.queue} and bound it to ${this.callExchange} with routing key ${this.routingKey}`);

    const { consumerTag } = await this.channel.consume(this.queue, async (msg: Message | null) => {
      if (msg) {
        const shuttleMessage = new RabbitShuttleMessage(this.channel, msg, consumerTag);
        try {
          for (const listener of this.listeners.values()) {
            await listener(shuttleMessage);
          }
          shuttleMessage.ack();
        } catch (e) {
          shuttleMessage.nack();
        }
      }
    }, {});
  }

}
