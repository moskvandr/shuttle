import { Channel, Connection } from 'amqplib';

import { ConnectionItem } from '../ConnectionItem';

export interface RabbitMqConnectionItem extends ConnectionItem {
  connection: Connection;
  channel: Channel;
  isBlocked: boolean;
}
