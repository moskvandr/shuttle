import {Message, Options} from 'amqplib';
import { v4 as uuid } from 'uuid';

import { OutgoingCall } from '../OutgoingCall';
import { ShuttleMessageData } from '../ShuttleMessageData';
import { ResponseTimeoutError } from '../ResponseTimeoutError';

import { Subscription } from './Subscription';
import { RabbitShuttleMessage } from './RabbitShuttleMessage';

export class RabbitOutgoingCall extends Subscription implements OutgoingCall {

  protected transactions = new Map<string, {resolve: (data: any) => void, reject: (reason: any) => void, action: any}>();
  protected tasks = new Map<string, NodeJS.Timeout>();
  protected timeout?: number;
  protected queueException!: string;
  protected queueOptions: Options.AssertQueue = {autoDelete: true, exclusive: true};

  public call(payload: any[], context: Record<string, any>, action: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const transactionId = uuid();
      const message: ShuttleMessageData<any> = {
        meta: {
          transactionId,
          replyRoutingKey: this.instanceId
        },
        payload,
        context,
      };
      this.transactions.set(transactionId, {resolve, reject, action});
      this.channel.publish(
        this.callExchangeFanout,
        '',
        Buffer.from(JSON.stringify(message)),
        this.getPublishOptions(),
      );
      this.addTimeoutTask(transactionId);
    });

  }

  public setTimeout(timeout?: number) {
    this.timeout = timeout;
  }

  protected addTimeoutTask(transactionId: string) {
    if (this.timeout) {
      this.tasks.set(
        transactionId,
        setTimeout(() => {
          const { reject } = this.transactions.get(transactionId) || {};
          this.deleteTimeoutTask(transactionId);
          this.transactions.delete(transactionId);
          reject?.(new ResponseTimeoutError());
        }, this.timeout),
      );
    }
  }

  protected deleteTimeoutTask(transactionId: string) {
    if (this.tasks.has(transactionId)) {
      clearTimeout(this.tasks.get(transactionId)!);
      this.tasks.delete(transactionId);
    }
  }

  protected getPublishOptions(): Options.Publish {
    return {
      expiration: this.timeout,
    }
  }

  protected async initQueue(): Promise<void> {
    await this.initQueueReply();
    await this.initQueueException();
  }

  protected async initQueueReply(): Promise<void> {
    const queue = this.queue = `${this.replyExchangeFanout}:queue:${uuid()}`;
    await this.channel.assertQueue(queue, this.queueOptions);
    await this.channel.bindQueue(queue, this.replyExchangeDirect, this.instanceId);
    const { consumerTag } = await this.channel.consume(queue, async (m: Message | null) => {
      await this.callReplyHandler(m, consumerTag);
    });
  }

  protected async initQueueException(): Promise<void> {
    const queue = this.queueException = `${this.exceptionExchangeFanout}:queue:${uuid()}`;
    await this.channel.assertQueue(queue, this.queueOptions);
    await this.channel.bindQueue(queue, this.exceptionExchangeDirect, this.instanceId);
    const { consumerTag } = await this.channel.consume(queue, async (m: Message | null) => {
      await this.callReplyHandler(m, consumerTag, true);
    });
  }

  protected async callReplyHandler(msg: Message | null, consumerTag: string, isException = false) {
    if (msg) {
      const shuttleMessage = new RabbitShuttleMessage(this.channel, msg, consumerTag);
      const data = shuttleMessage.get();
      const transactionId = data.meta.transactionId;

      if (this.transactions.has(transactionId)) {
        const { resolve, reject, action } = this.transactions.get(transactionId)!;
        if (!isException) {
          if (action) {
            const replyContext = shuttleMessage.getContext();
            Object.entries(replyContext).forEach(([propertyName, value]) => action[`#remote:${propertyName}`] = value);
          }

          resolve(shuttleMessage.getPayload());
        } else {
          reject(shuttleMessage.getPayload());
        }

        this.transactions.delete(transactionId);
        this.deleteTimeoutTask(transactionId);
      }

      this.channel.ack(msg);
    }
  }

  protected async initExchange() {
    //
  }

}
