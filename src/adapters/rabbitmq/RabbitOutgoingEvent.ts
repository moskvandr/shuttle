import { v4 as uuid } from 'uuid';

import { OutgoingEvent } from '../OutgoingEvent';
import { ShuttleMessageData } from '../ShuttleMessageData';

import { Subscription } from './Subscription';
// import {info} from "../../logging";

export class RabbitOutgoingEvent extends Subscription implements OutgoingEvent {

  protected setExchangeName() {
    this.callExchangeFanout = `${this.exchange}:event`;
    this.callExchange = `${this.callExchangeFanout}:topic`;
  }

  protected async initExchange() {
    await this.assertExchanges(this.callExchangeFanout, this.callExchange, 'topic');
  }

  public async emit(payload: any, routingKey: string): Promise<boolean> {
    const transactionId = uuid();
    const message: ShuttleMessageData<any> = {
      meta: {
        transactionId,
      },
      payload,
      context: {},
    };
    return this.channel.publish(
      this.callExchangeFanout,
      routingKey,
      Buffer.from(JSON.stringify(message)),
    );
  }

  protected async initQueue(): Promise<void> {
    // not necessary
  }

}
