import { Reaction } from '../../reaction/Reaction';
import { RabbitMqConnectionItem } from './RabbitMqConnectionItem';
import {error, info} from "../../logging";
import {Message} from "amqplib";

export class RabbitReactions extends Reaction<RabbitMqConnectionItem> {

  public onConnectionClose() {
    info(`Shuttle: connection ${this.item.url} is closed.`);
    this.removeMe();
  }

  public onConnectionError(err: Error) {
    error(`Shuttle: connection ${this.item.url} error: ${JSON.stringify(err)}`);
    this.removeMe();
  }

  public onConnectionBlocked(reason: string) {
    info(`Shuttle: connection ${this.item.url} is blocked by reason ${reason}`);
  }

  public onConnectionUnblocked() {
    info(`Shuttle: connection ${this.item.url} is unblocked`);
  }

  public onChannelClose() {
    info(`Shuttle: channel ${this.item.url} is closed.`);
    this.removeMe();
  }

  public onChannelError(err: Error) {
    error(`Shuttle: channel ${this.item.url} error: ${JSON.stringify(err)}`);
    this.removeMe();
  }

  public onChannelReturn(message: Message) {
    info(`Shuttle: channel ${this.item.url} received a non-routed message.`);
  }

  public onChannelDrain() {
    info(`Shuttle: channel ${this.item.url} is ready.`);
  }

  protected init() {
    this.initOnConnectionClose();
    this.initOnConnectionError();
    this.initOnConnectionBlocked();
    this.initOnConnectionUnblocked();
    this.initOnChannelClose();
    this.initOnChannelError();
    this.initOnChannelReturn();
    this.initOnChannelDrain();
  }


  protected initOnConnectionClose() {
    this.item.connection.on('close', () => {
      this.onConnectionClose();
    });
  }

  protected initOnConnectionError() {
    this.item.connection.on('error', (err: Error) => {
      this.onConnectionError(err);
    });
  }

  protected initOnConnectionBlocked() {
    this.item.connection.on('blocked', (reason: string) => {
      this.onConnectionBlocked(reason);
    });
  }

  protected initOnConnectionUnblocked() {
    this.item.connection.on('unblocked', (reason: string) => {
      this.onConnectionUnblocked();
    });
  }

  protected initOnChannelClose() {
    this.item.channel.on('close', () => {
      this.onChannelClose();
    });
  }

  protected initOnChannelError() {
    this.item.channel.on('error', (err: Error) => {
      this.onChannelError(err);
    });
  }

  protected initOnChannelReturn() {
    this.item.channel.on('return', (message: Message) => {
      this.onChannelReturn(message);
    });
  }

  protected initOnChannelDrain() {
    this.item.channel.on('drain', () => {
      this.onChannelDrain();
    });
  }

}
