import { ShuttleMessage } from '../ShuttleMessage';
import {Channel, Message} from "amqplib";

export class RabbitShuttleMessage extends ShuttleMessage {

  protected data: any;

  constructor(protected readonly channel: Channel, protected readonly message: Message, protected readonly consumerTag: string) {
    super();
    try {
      this.data = JSON.parse(message?.content.toString());
    } catch (e) {
      //
    }
  }

  public get(): any {
    return this.data;
  }

  public getPayload(): any {
    return this.data.payload;
  }

  public getContext(): Record<string, any> {
    return this.data.context;
  }

  // public getTransactionId(): string {
  //   return this.data.meta.transactionId;
  // }

  public ack() {
    this.channel.ack(this.message)
  }

  public nack() {
    try {
      this.channel.nack(this.message);
    } catch (e: any) {
      // ну с кем не бывает...
    }
  }

}
