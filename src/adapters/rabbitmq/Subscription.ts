import { v4 as uuid } from 'uuid';
import { Channel, Message, Options } from 'amqplib';
// import { EventEmitter } from 'events';

import { RabbitMqConnectionItem } from './RabbitMqConnectionItem';
import { info } from '../../logging';

// import { ShuttleMessage } from '../ShuttleMessage';

export abstract class Subscription {

  public readonly instanceId = uuid();

  protected channel!: Channel;
  protected callExchangeFanout!: string;
  protected callExchange!: string;
  protected replyExchangeFanout!: string;
  protected replyExchangeDirect!: string;
  protected exceptionExchangeFanout!: string;
  protected exceptionExchangeDirect!: string;
  protected queue!: string;

  protected exchangeOptions: Options.AssertExchange = {};
  protected queueOptions: Options.AssertQueue = {autoDelete: true};

  constructor(
    protected readonly conn: RabbitMqConnectionItem,
    protected readonly exchange: string
  ) {
    // super();
    this.setExchangeName();
  }

  public async init() {
    await this.setChannel();
    await this.initExchange();
    await this.initQueue();
  }

  public async finish(): Promise<void> {
    await this.channel.close();
  }

  protected async setChannel() {
    this.channel = this.conn.channel;
  }

  protected setExchangeName() {
    this.callExchangeFanout = `${this.exchange}:call`;
    this.callExchange = `${this.callExchangeFanout}:direct`;
    this.replyExchangeFanout = `${this.exchange}:reply`;
    this.replyExchangeDirect = `${this.replyExchangeFanout}:direct`;
    this.exceptionExchangeFanout = `${this.exchange}:exception`;
    this.exceptionExchangeDirect = `${this.exceptionExchangeFanout}:direct`;
  }

  protected async initExchange() {
    await Promise.all([
      this.assertExchanges(this.callExchangeFanout, this.callExchange),
      this.assertExchanges(this.replyExchangeFanout, this.replyExchangeDirect),
      this.assertExchanges(this.exceptionExchangeFanout, this.exceptionExchangeDirect),
    ]);
  }

  protected abstract initQueue(): Promise<void>;

  protected async assertExchanges(fanoutExchange: string, targetExchange: string, type: 'direct' | 'topic' = 'direct') {
    await this.channel.assertExchange(fanoutExchange, 'fanout', this.exchangeOptions);
    await this.channel.assertExchange(targetExchange, type, this.exchangeOptions);
    await this.channel.bindExchange(targetExchange, fanoutExchange, '');
    info(`Shuttle: ${this.conn.url} asserted 'fanout' exchange ${fanoutExchange}, '${type}' exchange ${targetExchange}. And bound ${targetExchange} to ${fanoutExchange}`);
  }


}
