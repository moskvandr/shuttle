import { connect } from 'amqplib';

import { info } from '../../logging';
import type { Settings } from '../Settings';
import type { connectionItemFactory } from '../ConnectionItem';
import { addReactionByName } from '../../reaction';

import { RabbitMqConnectionItem } from './RabbitMqConnectionItem';
import { RabbitOutgoingCall } from './RabbitOutgoingCall';
import { RabbitIncomingCall } from './RabbitIncomingCall';
import { RabbitReactions } from './RabbitReactions';
import {RabbitOutgoingEvent} from "./RabbitOutgoingEvent";
import {RabbitIncomingEvent} from "./RabbitIncomingEvent";

addReactionByName('rabbitmq-default', RabbitReactions);

export const createConnectionItem: connectionItemFactory = async (url: string, settings: Settings): Promise<RabbitMqConnectionItem> => {
  const connection = await connect(url);
  const channel = await connection.createChannel();
  await channel.prefetch(settings.prefetchCount!);

  info(`Shuttle: connected to ${url}`);

  const item: RabbitMqConnectionItem = {
    url,
    connection,
    channel,
    settings,
    isBlocked: false,
    disconnect: async () => {
      await channel.close();
      await connection.close();
    },
    async createOutgoingCall(exchange, timeout) {
      const adapter = new RabbitOutgoingCall(item, exchange);
      adapter.setTimeout(timeout);
      await adapter.init();
      return adapter;
    },
    async createIncomingCall(exchange, onMessage) {
      const adapter = new RabbitIncomingCall(item, exchange, onMessage);
      await adapter.init();
      info(`Shuttle: ${item.url} created exchanges and queue for remote calling ${exchange}.`);
      return adapter;
    },
    async createOutgoingEvent(exchange) {
      const adapter = new RabbitOutgoingEvent(item, exchange);
      await adapter.init();
      return adapter;
    },
    async createIncomingEvent(exchange, rotingKey: string, packStore: Record<string, any>, onMessage) {
      const adapter = new RabbitIncomingEvent(item, exchange, rotingKey, packStore, onMessage);
      await adapter.init();
      info(`Shuttle: ${item.url} created exchanges and queue for incoming event ${exchange}.`);
      return adapter;
    },
  };

  return item;
}
