import 'reflect-metadata';

import { actionConfigStore } from '../utils/actionConfigStore';
import { ActionConfiguration, ActionConfigurationParams } from '../interfaces';

export function action(cfg?: Partial<ActionConfiguration>) {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return (constructor: Function) => {
    if (cfg) {
      actionConfigStore.init(constructor as any);
      actionConfigStore.setSection(constructor as any, 'actionConfiguration', cfg);
    }
  }
}
