import 'reflect-metadata';

import { actionConfigStore } from '../utils/actionConfigStore';
import { ActionStuffing } from '../interfaces/ActionStuffing';
import { ContextConfiguration } from '../interfaces/ContextConfiguration';

const addManifest = (target: any, key: string, options: any) => {
  const manifest = actionConfigStore.getManifest(target);
  manifest.properties[key] = options;
}

const addContextConfig = (target: ActionStuffing, key: string, cfg?: Partial<ContextConfiguration>) => {
  actionConfigStore.init(target);
  const cc = actionConfigStore.getContextConfiguration(target);
  if (cfg) {
    cc[key] = cfg;
  }
}



export function context(config?: ContextConfiguration) {
  return (target: any, key: string) => {

    const cfg: any = {};

    if (config) {

      // eslint-disable-next-line no-prototype-builtins
      if (config.hasOwnProperty('defaultValue')) {
        cfg.defaultValue = config.defaultValue;
        target[key] = config.defaultValue;
      }

    }

    const type = Reflect.getMetadata("design:type", target, key);
    addContextConfig(target, key, config);
    addManifest(target, key, {
      type: type.name,
      ...config,
      ...cfg,
    });
  }
}
