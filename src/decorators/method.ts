import 'reflect-metadata';
import { ZodError, ZodTuple, ZodIssue, ZodType, ZodSchema } from 'zod';

import { actionConfigStore } from '../utils/actionConfigStore';
import { ActionStuffing, CallParamsValidator, MethodConfiguration, MethodManifest } from '../interfaces';

export class InvalidCallParametersError extends Error {
  public isZodError = true;
  constructor(public issues: ZodIssue[]) {
    super('Parameter check error');
  }
}

export const CallParamsValidatorStore = new Map<string, CallParamsValidator>();

const zodErrorToInvalidCallParametersError = (error: any) => {
  if (error instanceof ZodError) {
    return new InvalidCallParametersError(error.issues);
  }
  return error;
}

const initStringValidator = (cfg: Partial<MethodConfiguration>): boolean => {
  if (typeof cfg.params === 'string') {
    if (!CallParamsValidatorStore.has(cfg.params)) {
      throw new Error(`Call params validator ${cfg.params} not found.`);
    }
    cfg.paramsValidator = CallParamsValidatorStore.get(cfg.params);
    return true;
  }
  return false;
}

const initFunctionValidator = (cfg: Partial<MethodConfiguration>): boolean => {
  if (typeof cfg.params === 'function') {
    cfg.paramsValidator = cfg.params;
    return true;
  }
  return false;
}

const initZodTupleValidator = (cfg: Partial<MethodConfiguration>): boolean => {
  if (cfg.params instanceof ZodTuple) {
    const schema = cfg.params;
    cfg.paramsValidator = async (params): Promise<any[]> => {
      try {
        return schema.parseAsync(params);
      } catch (e) {
        throw zodErrorToInvalidCallParametersError(e);
      }
    }
    return true;
  }
  return false;
}

const initArrayZodSchemasValidator = (cfg: Partial<MethodConfiguration>): boolean => {
  if (Array.isArray(cfg.params)) {
    cfg.params.forEach((p) => {
      if (!(p instanceof ZodSchema)) {
        throw new Error(`Param config error bla bla`);
      }
    });
    const schemas = cfg.params;
    cfg.paramsValidator = async (params, action, mlc): Promise<any[]> => {
      const repeatSchemeFrom = mlc.getArgumentNames().findIndex((name) => name.substring(0, 3) === '...');
      const parsedParams: any[] = [];
      const issues: ZodIssue[] = [];
      let currentScheme: ZodType<any> = schemas[0];
      for (const [index, param] of params.entries()) {
        if (index <= repeatSchemeFrom || repeatSchemeFrom === -1) {
          currentScheme = schemas[index];
        }
        try {
          parsedParams.push(await currentScheme.parseAsync(param));
        } catch (e) {
          if (e instanceof ZodError) {
            issues.push(...e.issues.map(issue => {
              issue.path.push(index.toString(10));
              return issue;
            }));
          }
          throw new InvalidCallParametersError(issues);
        }
      }
      return parsedParams;
    }
    return true;
  }
  return false;
}

const initValidatorList = [initStringValidator, initFunctionValidator, initZodTupleValidator, initArrayZodSchemasValidator];

const initValidator = (cfg: Partial<MethodConfiguration>) => {
  if (cfg.params) {
    for (const initVld of initValidatorList) {
      if (initVld(cfg)) {
        break;
      }
    }
  }
}

const addMethodManifest = (target: ActionStuffing, key: string, options: MethodManifest) => {
  actionConfigStore.init(target);
  const manifest = actionConfigStore.getManifest(target);
  manifest.methods[key] = options;
}

const addMethodConfig = (target: ActionStuffing, key: string, cfg?: Partial<MethodConfiguration>) => {
  actionConfigStore.init(target);
  const mc = actionConfigStore.getMethodConfiguration(target);
  if (cfg) {
    initValidator(cfg);
    mc[key] = cfg;
  }
}

const getArguments = (fn: any): string[] => {
  const strFn = fn.toString()
    .replace(/^async /i, '');
  const fnHeader = strFn.match(/^[a-z0-9_]+(?:\s|)\((.*?)\)/gi);

  if (fnHeader && fnHeader[0]) {
    if (fnHeader[0].indexOf('=') !== -1) {
      throw new Error(`Default values are not supported (fn: ${strFn})`);
    }
    return fnHeader[0].replace(/^[a-z0-9_]+(?:\s|)\(/gi, '')
      .replace(/\)/g, '')
      .split(', ').filter((v: string) => !!v);
  }

  return [];
}

export function method(cfg?: Partial<MethodConfiguration>) {
  return (target: any, key: string, descriptor: PropertyDescriptor) => {
    const methodReturnType = Reflect.getMetadata('design:returntype', target, key);

    if (methodReturnType !== Promise) {
      throw new Error(`Method ${key} of the ${target.constructor.name} class must return a promise.`);
    }

    const mm: MethodManifest = {args: getArguments(target[key])};

    if (typeof cfg?.params === 'string') {
      mm.paramsValidatorFactoryName = cfg.params;
    }

    addMethodConfig(target, key, cfg);
    addMethodManifest(target, key, mm);

  }
}
