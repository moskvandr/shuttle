let infoFn = console.info;
let errorFn = console.error;

export function info(...args: any) {
  infoFn(...args);
}

export function error(...args: any) {
  errorFn(...args);
}

export function setInfoFn(fn: (...args: any) => any) {
  infoFn = fn;
}

export function setErrorFn(fn: (...args: any) => any) {
  errorFn = fn;
}
