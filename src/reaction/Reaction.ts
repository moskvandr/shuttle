import type { ConnectionItem } from '../adapters/ConnectionItem';

export class Reaction<T extends ConnectionItem> {

  constructor(protected item: T, protected removeMe: () => void) {
    this.init();
  }

  protected init() {
    // bla bla
  }

}
