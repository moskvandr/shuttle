import type { Reaction } from './Reaction';


const reactions = new Map<string, typeof Reaction>();

export function getReactionByName(name: string): typeof Reaction {
  const reactionClass = reactions.get(name);
  if (!reactionClass) {
    throw new Error(`Reaction ${name} is not found.`);
  }
  return reactionClass;
}

export function addReactionByName(name: string, reactionClass: any) {
  reactions.set(name, reactionClass);
}
