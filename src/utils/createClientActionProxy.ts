import type { ConnectionItem } from '../adapters/ConnectionItem';
import type { ShuttleRemoteActionBackbone } from '../ShuttleRemoteActionBackbone';
import { ShuttleEventClientPack } from '../ShuttleEventClientPack';
import type { ShuttleTrip } from '../ShuttleTrip';

export async function createClientActionProxy<T extends Record<string, any>>(
  conn: ConnectionItem,
  backbone: ShuttleRemoteActionBackbone,
  context: Record<string, any>,
  trip?: ShuttleTrip,
): Promise<T> {

  const ctx: Record<string, any> = {...backbone.getDefaultContext(), ...context};
  const eventPack = new ShuttleEventClientPack(conn, backbone);

  const finish = async () => {
    await eventPack.finish();
  }

  const actionProxy = new Proxy<T>(ctx as any, {
    get(target, key: string) {

      // remote method
      if (backbone.adapters.has(key)) {
        const a = backbone.adapters.get(key);
        return (...payload: any[]) => a!.call(payload, ctx, getActionProxy());
      }

      if (eventPack.has(key)) {
        return eventPack.get(key);
      }

      if (key === ':finish') {
        return finish;
      }

      return target[key as keyof T];
    },
    set(target: T, keyIn: string, value: any, receiver: any): boolean {
      let share = false;
      let remote = false;
      let key = keyIn;

      if (keyIn.substring(0, 7) === '#share:') {
        key = keyIn.substring(7);
        share = true;
      } else if (keyIn.substring(0, 8) === '#remote:') {
        key = keyIn.substring(8);
        remote = true;
      }

      if (!remote && !share && backbone.contextPropertyIsReadOnly(key)) {
        return false;
      }

      if (remote && backbone.contextPropertyIsShareOnReply(key) && trip) {
        trip.shareContext(key, value);
        return true;
      }

      ctx[key] = value;
      return true;
    }
  });

  function getActionProxy(): any {
    return actionProxy;
  }

  await eventPack.init(actionProxy);
  return actionProxy;
}
