// import { info } from '../logging';
//
// import { RabbitIncomingCall } from '../adapters/rabbitmq/RabbitIncomingCall';
// import type { IncomingCall } from '../adapters/IncomingCall';
// import type { ShuttleMessage } from '../adapters/ShuttleMessage';
// import type { ConnectionItem } from '../adapters/ConnectionItem';
//
// export async function createIncomingCallAdapter(conn: ConnectionItem, exchange: string, onMessage: (message: ShuttleMessage) => void): Promise<IncomingCall> {
//   const adapter = new RabbitIncomingCall(conn as any, exchange, onMessage);
//   await adapter.init();
//   info(`Shuttle: ${conn.url} created exchanges and queue for remote calling ${exchange}.`);
//   return adapter;
// }
