import { ShuttleEvent } from '../ShuttleEvent';
import { ActionManifest } from '../interfaces';



export function createServerActionProxy<T extends Record<string, any>>(manifest: ActionManifest, action: T, context: Record<string, any>): T {
  const eventProxyCash: {[key: string]: ShuttleEvent<any>} = {};
  const actionProxy = new Proxy<T>(action, {
    get(target, key: string) {
      // eslint-disable-next-line no-prototype-builtins
      if (context.hasOwnProperty(key)) {
        return context[key];
      }
      if (target[key] instanceof ShuttleEvent) {
        const e = target[key];
        if (!eventProxyCash[key]) {
          eventProxyCash[key] = new Proxy<any>(e, {
            get(targetEvent, key: string) {
              return key === 'action'? actionProxy : targetEvent[key];
            }
          });
        }
        return eventProxyCash[key];
      }
      return target[key];
    },
    set(target: T, key: string, value: any): boolean {
      context[key] = value;
      return true;
    }
  });
  return actionProxy;
}
