/* eslint-disable @typescript-eslint/ban-types */
import { suite, test, timeout } from '@testdeck/mocha';
import { expect } from 'chai';

import { ShuttleConnect } from '../../src';

import '../setup';


@suite('Подключение')
class ConnectingSpec {

  @test('Подключение с использованием конфига по умолчанию.')
  public async simpleConnectTest(): Promise<void> {
    const connection = await ShuttleConnect.getConnection();
    expect(connection).contain.keys(['channel', 'connection', 'url', 'disconnect']);
    await connection.disconnect();
  }
}
