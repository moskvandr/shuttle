/* eslint-disable @typescript-eslint/ban-types */
import { suite, test, timeout } from '@testdeck/mocha';
import { expect } from 'chai';

import { Shuttle } from '../../src';
import '../setup';

import { NoConfigAction } from './actions/NoConfigAction';
import { createServerShuttle } from './utils/createServerShuttle';
import { createClientShuttle } from './utils/createClientShuttle';
import { WithContext } from './actions/WithContext';
import { Car } from './actions/Car';


@suite('Создание клиентского экшена.')
class CreateInstanceOfRemoteActionSpec {

  private server!: Shuttle;

  public async before() {
    this.server = await createServerShuttle();
    await this.server.begin();
  }

  @test('Простое создание клиентского экшена.')
  public async createRemoteActionTest(): Promise<void> {
    const client = await createClientShuttle();
    const action = await client.createRemoteAction<NoConfigAction>('NoConfigAction');
    expect(action.m1).to.be.a('Function');
  }

  @test('Контекст: проверка полей со значением по умолчанию.')
  public async ctxDefaultPropsTest(): Promise<void> {
    const client = await createClientShuttle();
    const action = await client.createRemoteAction<WithContext>('WithContext');
    expect(action.propertyDefaultValue).to.be.equal(50);
  }

  @test('Контекст: установка значения для поля со значением по умолчанию во время создания экшена.')
  public async ctxDefaultPropsWithInitValueTest(): Promise<void> {
    const client = await createClientShuttle();
    const action = await client.createRemoteAction<WithContext>('WithContext', {propertyDefaultValue: 100});
    expect(action.propertyDefaultValue).to.be.equal(100);
  }

  @test('Контекст: проверяем поле помеченное только для чтения на клиенте.')
  public async ctxPropsReadOnlyTest(): Promise<void> {
    const client = await createClientShuttle();
    const action = await client.createRemoteAction<WithContext>('WithContext', {propertyReadonly:  100});
    expect(action.propertyReadonly).to.be.equal(100, 'readonlyOnClient поле можно устанавливать во время создания экшена.');
    expect(() => action.propertyReadonly = 50).to.be.throw();
  }

  @test('Создание экшена с использованием параметров.')
  public async createWithParamTest(): Promise<void> {
    const client = await createClientShuttle();
    const action1 = await client.createRemoteAction<Car>('Car',  {sessionId: '55555'}, 100);
    const action2 = await client.createRemoteAction<Car>('Car',  {sessionId: '55555'}, 200);
    const msg = 'У этого экшена есть хук onClientConnect который должен отработать и установить новое значение по умолчанию для поля propertyOK.';
    expect(action1.propertyOK).to.be.equal(150, msg);
    expect(action2.propertyOK).to.be.equal(250, msg);
  }


  //
  // @test('Get remote action.')
  // public async getR1emoteActionTest(): Promise<void> {
  //   const server = new Shuttle();
  //   const car = new Car();
  //   await server.addAction('Car', car);
  //   await server.begin();
  //
  //
  //
  //   // server.on('actionAdded', (x: string) => {
  //   //   //
  //   // })
  //
  //
  //
  //   expect(car.propertyOK).to.be.equal(50);
  //
  //   const shuttle1 = new Shuttle();
  //   const client1 = await shuttle1.trip();
  //
  //   const shuttle2 = new Shuttle();
  //   const client2 = await shuttle2.trip();
  //
  //   const action1 = await client1.createRemoteAction<Car>('Car',  {sessionId: '55555'}, 50);
  //   const action2 = await client1.createRemoteAction<Car>('Car', {sessionId: '55555'}, 100); // здесь не будет использован аргумент так как никто не пойдет на сервер за этим экшеном
  //   const action3 = await client1.createRemoteActionExclusive<Car>('Car', {sessionId: '55556'}, 100); // а в этом месте будет т.к. кто-то обязательно сходит на сервер за этим экшеном
  //
  //   const action4 = await client2.createRemoteAction<Car>('Car',  {sessionId: '55555'}, 50);
  //
  //   expect(action1.open).to.be.a('Function');
  //   expect(action1.close).to.be.a('Function');
  //
  //   expect(action2.open).to.be.a('Function');
  //   expect(action2.close).to.be.a('Function');
  //
  //   expect(action1.koko).to.be.equal(10);
  //
  //   expect(action1.propertyOK).to.be.equal(100);
  //   expect(action2.propertyOK).to.be.equal(100);
  //   expect(action3.propertyOK).to.be.equal(150);
  //
  //
  //   action1.specialEventPath.place.yyy = 'up';
  //
  //   action2.propertyOK = 555;
  //   action2.specialEventPath.place.yyy = 'up';
  //
  //
  //   expect(action1.propertySharedOnClient).to.be.equal(1);
  //   expect(await action1.setPropertySharedOnClient(2)).to.be.equal(2);
  //   expect(action1.propertySharedOnClient).to.be.equal(2);
  //   expect(action2.propertySharedOnClient).to.be.equal(2);
  //   expect(action4.propertySharedOnClient).to.be.equal(1); // этот типа едет на отдельном автобусе
  //
  //   expect(action3.propertySharedOnClient).to.be.equal(1); // это эксклюзивная херня она как-бы бежит за автобусом
  //
  //
  //   const action5 = await client1.createRemoteAction<Car>('Car');
  //   expect(action5.propertySharedOnClient).to.be.equal(2); // Этот экшен появился уже после установки шаренного поля но он должне получить новое значение
  //
  //
  //
  //   // await action1.checkParams(1, 'JJJIIIFFF'); //надо проверять что ошибка более или менее указывает на место в котором это случилось
  //
  //

  //
  //
  //   try {
  //     action1.propertyReadonlyOnClient = 'WROTE';
  //     expect(true).to.be.false;
  //   } catch (e) {
  //     //
  //   }
  //
  //
  //   const openResult = await action1.open();
  //   expect(openResult).to.be.false;
  //
  //   // Оказалось что методу rejectedWith похеру вернет вызываемый метод значение или кинет исключение...
  //   try {
  //     await action2.ex();
  //     expect(false).to.be.true;
  //   } catch (e: any) {
  //     expect(e.message).contain('remote');
  //   }
  //
  //   //const schRes1 = await action2.fnParamSchemaValidator('5' as any, 'false' as any);
  //
  //
  //   try {
  //     const schRes2 = await action2.fnParamArrSchemasValidator(15, true, 'zz', 'kkkkk');
  //     console.log(schRes2);
  //   } catch (e) {
  //     console.log(JSON.stringify(e, null, 2));
  //   }
  //
  //   await expect(action3.ex()).to.be.rejectedWith(/remote/);
  //
  //
  //   expect(action1.koko).to.be.equal(10);
  //   try {
  //     const r = await action1.fnParamStrValidator(2000, false);
  //   } catch (e) {
  //     console.log('VALIDATION PARAM ERR: ', e);
  //   }
  //
  //   expect(action1.koko).to.be.equal(200);
  //
  //
  //
  //   try {
  //     const r = await action1.fnParamValidator(2, false, '1', '2', '3');
  //   } catch (e) {
  //     console.log('VALIDATION PARAM ERR: ', e);
  //   }
  //
  //   await action1.superEvent.on((data, action) => {
  //     console.log('SUPER EVENT A1', data, action === action1);
  //   });
  //
  //   await action2.superEvent.on((data, action) => {
  //     console.log('SUPER EVENT A2 ЭТОТ ДОЛЖЕН СЛУЧИТЬСЯ ОДНАЖДЫ (ПОПРОБУЕМ ЕГО ГРОХНУТЬ)', data, action === action2);
  //   });
  //
  //   await action3.superEvent.on((data, action) => {
  //     console.log('SUPER EVENT A3 ЭТОГО БЫТЬ НЕ ДОЛЖНО', data, action === action3);
  //   });
  //
  //
  //   await action1.specialEvent.on((data, action) => {
  //     console.log('SPECIAL EVENT A1', data, action === action1);
  //   });
  //
  //   await action2.specialEvent.on((data, action) => {
  //     console.log('SPECIAL EVENT A2', data, action === action2);
  //   });
  //
  //   await action3.specialEvent.on((data, action) => {
  //     console.log('SPECIAL EVENT A3', data, action === action3);
  //   });
  //
  //
  //
  //   console.log('SUPER SUPER EVENT FIRED:', await action1.fireSuperEvent());
  //   console.log('SPECIAL SUPER EVENT FIRED:', await action1.fireSpecialEvent());
  //
  //   await client1.finish(action2);
  //
  //   await action1.fireSuperEvent();
  //
  //   const schRes2 = await action2.fnParamArrSchemasValidator(15, true, 'OO', 'DDDDD');
  //   console.log(schRes2);
  //
  //   const arrSmlRes1 = await action2.fnParamArrSmplSchemasValidator(15, true, 'OO');
  //   console.log(arrSmlRes1);
  //
  //
  //
  // }
  //
  // @test('Get nonexistent remote action.')
  // public async getNonExistentRemoteActionTest(): Promise<void> {
  //   const shuttle1 = new Shuttle();
  //   const client1 = await shuttle1.trip();
  //   await expect(client1.createRemoteAction<Car>('NoCar', {}, null)).to.be.rejectedWith(/404/);
  // }

}
