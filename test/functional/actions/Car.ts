import { z } from 'zod';

import { action, context, method, CallParamsValidatorStore } from '../../../src';

import { ShuttleEvent } from '../../../src/ShuttleEvent';

CallParamsValidatorStore.set('superJopoParamsValidator', async (params: any[], action): Promise<void> => {
  if (params[0] < 1000) {
    throw new Error('Первый параметр должен быть больше тыщи...');
  }
})

export interface SuperEventData {
  name: string;
  description: string;
}


@action({
  timeout: 1000,
  async onClientConnect(alc, manifest, clientParams: number): Promise<void > {
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    if (clientParams) {
      const prop = manifest.properties.propertyOK;
      const dv = prop.defaultValue as number;
      if (dv) {
        prop.defaultValue = dv + clientParams;
      }
    }
    return;
  },
  async onContextReady(alc, action): Promise<void> {
    action.gogo = action.propertyOK - 1;
  }
})
export class Car {

  @context({
    defaultValue: 50,
    schema: z.number()

  })
  public propertyOK!: number;

  @context({
    schema: z.enum(['READONLY', 'WROTE']),
    defaultValue: 'READONLY',
    reply: true,
    readonlyOnClient: true,
  })
  public propertyReadonlyOnClient!: 'READONLY' | 'WROTE';


  @context({
    schema: z.number(),
    defaultValue: 1,
    reply: true,
    readonlyOnClient: true,
    shareOnReply: true,
  })
  public propertySharedOnClient!: number;


  @context({
    defaultValue: 10,
    reply: true
  })
  public koko!: number;

  @context({
    defaultValue: {
      place: {
        xxx: 'defaultLeft',
        yyy: 'defaultRight',
      },
    },
    reply: true
  })
  public specialEventPath!: {
    place: {
      xxx: string;
      yyy: string;
    };
  };

  @context({
    schema: z.string().min(5).max(5),
  })
  public sessionId!: string;

  public gogo = 0;

  public superEvent = new ShuttleEvent<SuperEventData>([':sessionId']);
  public specialEvent = new ShuttleEvent<SuperEventData>([':specialEventPath.place.yyy', ':specialEventPath.place.xxx']);


  @method()
  public async open(): Promise<boolean> {
    return false;
  }

  @method()
  public async close(): Promise<boolean> {
    return false;
  }

  @method({
    params: z.tuple([
      z.number().min(5),
      z.string().max(5),
    ])
  })
  public async checkParams(mew: number, wof: string): Promise<boolean> {
    return false;
  }

  @method()
  public async ex(): Promise<boolean> {
    throw new Error('Custom remote method error!');
  }

  @method()
  public async fireSuperEvent(): Promise<boolean> {
    return this.superEvent.emit({name: 'Привет!', description: 'Пробное событие'});
  }

  @method()
  public async fireSpecialEvent(): Promise<boolean> {
    return this.specialEvent.emit({name: 'Привет!', description: 'Специальное событие событие'});
  }

  @method({
    // params: z.tuple([
    //   z.number().min(100).max(999),
    //   z.boolean(),
    //
    // ])
  })
  public async args(x: number, b: boolean, ...s: string[]): Promise<boolean> {
    return false;
  }

  @method({
    async params(params: any[], action): Promise<void> {
      if (action.propertyOK !== 100) {
        throw new Error('Не работает установка контекста для прокси')
      }
      if (action.propertyOK === 100 && params[0] === 1) {
        throw new Error('OPOJ')
      }
    },
  })
  public async fnParamValidator(x: number, b: boolean, ...s: string[]): Promise<string> {
    return 'JOPO';
  }

  @method({
    params: z.tuple([
      z.number().min(10),
      z.boolean(),
    ])
  })
  public async fnParamSchemaValidator(x: number, b: boolean, ...s: string[]): Promise<string> {
    return `JOPO-${x}-${b}-${this.gogo}`;
  }

  @method({
    params: [
      z.number().min(10),
      z.boolean(),
      z.string().max(5),
    ]
  })
  public async fnParamArrSmplSchemasValidator(x: number, b: boolean, m: string): Promise<string> {
    return `JOPO-${x}-${b}-${m}-${this.gogo}`;
  }

  @method({
    params: [
      z.number().min(10),
      z.boolean(),
      z.string().max(5),
    ]
  })
  public async fnParamArrSchemasValidator(x: number, b: boolean, ...s: string[]): Promise<string> {
    return `JOPO-${x}-${b}-${this.gogo}`;
  }

  @method({
    params: 'superJopoParamsValidator'
  })
  public async fnParamStrValidator(x: number, b: boolean): Promise<string> {
    this.koko = 200;
    return `JOPO-${x}-${b}-${this.gogo}`;
  }

  @method()
  public async setPropertySharedOnClient(x: number): Promise<number> {
    this.propertySharedOnClient = x;
    return this.propertySharedOnClient;
  }


}
