import { context, method } from '../../../src';

export class NoConfigAction {


  @context({
    defaultValue: 'BLA',
    readonlyOnClient: true,
    shareOnReply: true,
  })
  public sharedValue!: string;

  @method()
  public async m1(): Promise<boolean> {
    return true;
  }
  @method()
  public async raise(p: string): Promise<boolean> {
    return false;
  }

  @method()
  public async setSharedValue(value: string): Promise<string> {
    return this.sharedValue = value;
  }
}
